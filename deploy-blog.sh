#!/bin/bash

pelican ./content -t ../bricks-theme -v -o ../bricks.lamboz.net/__site/ 
cd ../bricks.lamboz.net/ 
git add . 
git commit -m "$1" 
git push
